package com.puzzles.cipher;

public class CodeBody {

    private final String decodedText;
    private final String encodedText;
    private CipherKey ck;



    public CodeBody(String text, String keyWord) {
        this.decodedText = text.toUpperCase();
        this.ck = CipherKey.getInstance(keyWord.toUpperCase());
        this.encodedText = encodeText();
    }

    private String encodeText(){
        char[] codeChars = ck.getKeyOffsetStore();

        char[] encodeChars = new char[decodedText.length()];
        int charCount = 0;
        int stringLength = 0;
        int intOfCharInKey;
        char oneCharOfKey;
        int increment;

        for (char c: decodedText.toCharArray()) {
            stringLength ++;
            if (c != ' ') {
                charCount++;

                intOfCharInKey = charCount % ck.getLength();
                if (intOfCharInKey != 0) {
                    oneCharOfKey = codeChars[intOfCharInKey - 1];
                } else {
                    oneCharOfKey = codeChars[ck.getLength()-1];
                }
                increment = (Character.getNumericValue(oneCharOfKey) - 10);

                int newIntValue = (increment + (Character.getNumericValue(c) - 9));

                if (newIntValue > 26){
                    newIntValue -= 26;
                }
                if (increment + (Character.getNumericValue(c) - 9) > 26) {
                    increment = increment - 26;

                }
                c += increment;
                encodeChars[stringLength-1] = c;
            } else {
                encodeChars[stringLength-1] = c;
            }
        }

        return String.copyValueOf(encodeChars);


    }

    public String getDecodedText() {
        return decodedText;
    }

    public String getEncodedText() {
        return encodedText;
    }
}
