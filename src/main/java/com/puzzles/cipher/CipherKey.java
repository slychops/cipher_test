package com.puzzles.cipher;

public class CipherKey {
    private static CipherKey instance = null;

    private int length;
    private final String code;
    private char[] keyOffsetStore;


    private CipherKey(String keyWord) {
        this.code = keyWord;
        this.length = keyWord.length();
        this.keyOffsetStore = keyWord.toCharArray();
    }

    public static CipherKey getInstance(String keyWord){
        if (instance == null){
            instance = new CipherKey(keyWord);
        }
        return instance;
    }

    public String getCode() {
        return code;
    }

    public int getLength() {
        return length;
    }

    public char[] getKeyOffsetStore() {
        return keyOffsetStore;
    }
}
